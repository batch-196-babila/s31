/* 
    Client is an app which creates requests for resources from a server.
    Client trigger an action, in web dev. context, through URL and wait for the response of the server

    Server is able to host and deliver resources requested by a client

    Node.js is a runtime environment which allows us to create/develop backend/server-side apps with JS. By default, JS was conceptualized solely to the front end

    Why Popular?
    performance
    familiarity as uses JS as its language

    NPM
*/

// console.log("Hello Word!");

let http = require("http");

// require() is built in JS method allows to import package
// packages are pice of code we can integrate into our application

// http is a default package that comes with Node.js. Allows us to use mthods that let us create server

// http is a module. Modules are packages we imported
// Modules are object that contains codes, methods or data

// The http module let us create a server which is able to communicate with client though the use of Hypertext Transfer Protocol

// console.log(http);

http.createServer(function(request, response) {
    // console.log(request.url); // contains the URL endpoint

    if(request.url === "/") {
        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Hello from our first server!. This is from / endpoint");
    }
    else if (request.url === "/profile"){ // 2 roots
        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Hi! I'm Bryan!");
    }
}).listen(4000);

console.log("Server is running on localHost:4000!");

/*
    createServer() is a method from the http module that allows us to handle requests and response from a client and a server respectivelly

    createServer() method takes a function argument which is able to receive 2 object. 
        
    The request object which contains details of the "request" from the client. 
        
    The request object which contains details of the "response" from the client. 

    createServer() ALWAYS receive the request object first before the response regardless of the parameter

    response.writeHead() is a method of the response object. Allows us to add headers to our response. Headers are additional info about our response

    We have 2 arguments in our writeHead() method. The first is in the HTTP status code. An HTTP status is just a numerical code to let the client know about the status of their request. 200 means ok. 404 means cannot found

    Content-Type' recognizable headers and simply pertains to the data type of our response (text/plain)

    response.end() ends our response and send a message/data as a string.

    listen() allows us ti assugb a pirt ti our server. This method allows us to assigned port 400 to local machine.

    local host: --- Local machine : 4000 is port

    4000, 4040, 8000, 5000, 3000, 4200 are for web developement

    console.log() is added to show validation whihc port # we're in
        
*/

// protocol to client-server communication - http://localhost:4000 - server/app

// CTRL + C - turn off server

/*
    Servers can actually respond diferrently with different request
    We start our request with our URL. A client can start a different request with a different url
    http://localhost:4000 is not the same as http://localhost:4000/profile
    / url endpoirnt (default)
    ex: /profile

    We can differentiate requests by their endpoints, we should be able to respond diferrently to different endpoints

    console.log(request.url); // contains the URL endpoint
    / - default endpoint - request URL: http/localhost:4000
    /favicon/ico - browser's default behavior to retrieve the favicon
    /profile
    /favicon/ico
*/